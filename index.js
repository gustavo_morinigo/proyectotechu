// Utilizar nuevas funcionalidades del Ecmascript 6
//'use strict'

var express=require('express');
var bodyparser = require('body-parser');
const fetch = require('node-fetch');
// Cargamos el módulo de mongoose para poder conectarnos a MongoDB
var mongoose = require('mongoose');

var app = express(); 
app.use(bodyparser.json());

port = process.env.PORT || 3000;

let respuesta = {
  error: false,
  codigo: 200,
  mensaje: ''
 };

// Le indicamos a Mongoose que haremos la conexión con Promesas
mongoose.Promise = global.Promise;

// Se realiza la Conexión con la Base de datos y se levanta la aplicación
mongoose.connect('mongodb://localhost:27017/techu', {useNewUrlParser: true, promiseLibrary: global.Promise, useUnifiedTopology: true }, function(err, res) {
  if(err) {
    console.log('ERROR: connecting to Database. ' + err);
  }
  console.log('Connected to Database Usuarios');
  app.listen(3000, function() {
    console.log("Aplicación ejemplo, escuchando el puerto 3000. http://localhost:3000");
  });
}); 

// Import Models and controllers
var models     = require('./models/usuarios')(app, mongoose);
var UsuariosCtrl = require('./controllers/usuarios');

// Router ejemplo
//var router = express.Router();
//app.use(router);

app.get('/', function(req, res) {
  res.send("Hello world!");
});

/*app.get('/', function(req, res) {
    res.send('Hola Mundo3!');
});*/

// API routes
//var usuarios = express.Router();

app.get('/usuarios', function(req, res) {
    console.log(UsuariosCtrl.findAllUsuarios());
    res.send(UsuariosCtrl.findAllUsuarios());
});

app.post('/usuarios', function(req, res) {
  console.log("routeo post /usuarios");
  console.log("req body:" + req.body.algo);
  res.send(UsuariosCtrl.addUsuario(req));
  //res.send("post ok");
});

/*
app.route('/usuarios/:id')
  .get(UsuariosCtrl.findById)
  .put(UsuariosCtrl.updateUsuario)
  .delete(UsuariosCtrl.deleteUsuario);

app.use('/api', usuarios);

// en cada request http para cualquier método, se invoca esta funcion
app.use(function (req, res, next) {
    console.log('Time:', Date.now());
    next();
});*/


//Consulta de movimientos
app.get('/v1/movimientos', function(req, res) {
    res.sendfile('./data/movimientosv1.json'); 
})

//Consulta de usuarios
app.get('/v1/usuarios', function(req, res) {
    res.sendfile('./data/usuariosv1.json'); 
})

// Alta de Movimiento
app.post('/v1/movimientos', function(req, res) {
    var nuevo = req.body;
    console.log(nuevo.id);
    res.send('movimiento dado de alta');    
})

// Alta de Usuario
app.post('/v1/usuarios', function(req, res) {
    var nuevo = req.body;
    console.log(nuevo.id);
    res.send('usuario dado de alta');    
})

// Consulta Otras api
app.get('/v1/consapi', function(req, res) {
    fetch('https://pokeapi.co/api/v2/pokemon/ditto/')
    .then(resul => resul.json())
    .then(json => {
      console.log('enviando json ' + json.name);
      console.log(' ');
      res.send(json);
    });
})

