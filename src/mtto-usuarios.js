/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';

class MttoUsuarios extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
          padding: 10px;
        }
        <!--
        h1, h2 {display: inline;}
        -->
      </style>

      <div class="card">
        <h2 class="circle">1</h2>
        <h1>Mantenimiento Usuarios</h1>
        <div class="formulario">
            <p> nombre: <input type="text" id="nombre" required/></br> 
                apellido: <input type="text" id="apellido" required/></br>
                Fecha alta: <input type="date" id="fechaAlta" required/></br> 
                Dni: <input type="number" id="dni" required/></br> 
                correo electronico: <input type="email" id="email" required/></br>
            </p>
            <p> <input type="button" value="Alta/Modificación Usuario" id="BotonAltaUsuario" on-click="altaModifUsuario">
                <input type="button" value="Consultar Usuario" id="BotonConsultaUsuario" on-click="consultarUsuario">
            </p>
        </div>
      </div>
    `;
  }

  altaModifUsuario () {
    if (this.validarNombre()){
      var nombre = this.$.nombre.value;
      console.log("grabando " + nombre);
      alert('Alta del Usuario "' + nombre + '" realizada con éxito');
    }
  }

  validarNombre(){
    var nombre = this.$.nombre;
    if (nombre.value == "") {
      alert('Debe ingresar el nombre del Usuario');
      return false;
    }
    return true;
  }

  consultarUsuario () {
    /* obtengo los campos desde localStorage */
    //let nombre = localStorage.getItem('nombre');
    //let email = localStorage.getItem('email');
  
    console.log('consultando', nombre);
  
    /* Muevo los campos a pantalla */
    //document.getElementById('nombre').value = nombre;
    //document.getElementById('email').value = email;
  }
}

window.customElements.define('mtto-usuarios', MttoUsuarios);
