/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';

class ConsApiPok extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
          padding: 10px;
        }
        <!--
        h1, h2 {display: inline;}
        -->
      </style>

      <div class="card">
        <h2 class="circle">4</h2>
        <h1>Consultar Api Pokemon</h1>
        <div>
            <p> 
                Nombre Pokemon: <label for="Nombre Pokemon" id="nombrePokemon" ></label></br>
                Id: <label for="idPokemon" id="idPokemon" ></label> </br>
                Peso: <label for="pesoPokemon" id="pesoPokemon" ></label> </br>
                Consultado desde: <label for="consultadoDesde" id="consultadoDesde" ></label>
            </p>
            <p> 
                <input type="button" value="buscarDitto" id="buscarDitto" on-click="buscarDitto">
                <input type="button" value="buscarDittoApi" id="buscarDittoApi" on-click="buscarDittoApi">
            </p>

        </div>

        <p> Se consulta la Api: <a href="https://pokeapi.co/api/v2/pokemon/">https://pokeapi.co/api/v2/pokemon/</a></p>
        </div>
    `;
  }

  buscarDitto(){
    let url = 'https://pokeapi.co/api/v2/pokemon/ditto/';
    fetch(url)
    .then(res => res.json())
    .then(json => {
      console.log(json);
      let nombre = json.name;
      let id = json.id;
      let peso = json.weight;
      this.$.nombrePokemon.innerHTML = nombre;
      this.$.idPokemon.innerHTML = id;
      this.$.pesoPokemon.innerHTML = peso;
      this.$.consultadoDesde.innerHTML = 'BuscarDitto';
      });
  }
  
  buscarDittoApi(){
    let url = 'http://localhost:3000/v1/consapi';
    fetch(url)
    .then(res => res.json())
    .then(json => {
      console.log(json);
      let nombre = json.name;
      let id = json.id;
      let peso = json.weight;
      this.$.nombrePokemon.innerHTML = nombre;
      this.$.idPokemon.innerHTML = id;
      this.$.pesoPokemon.innerHTML = peso;
      this.$.consultadoDesde.innerHTML = 'BuscarDittoApiLocalHost';
      });
  }

}

window.customElements.define('cons-api-pok', ConsApiPok);
