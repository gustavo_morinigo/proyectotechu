exports = module.exports = function(app, mongoose) {
    var usuariosSchema = new mongoose.Schema({ 
        nombre:    { type: String },
        apellido:  { type: String },
        fechaAlta:   { type: Date },
        dni:      { type: Number },
        email:  { type: String }
    });
    mongoose.model('Usuarios', usuariosSchema);
};