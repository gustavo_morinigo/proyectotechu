//File: controllers/usuarios.js
var mongoose = require('mongoose');
var Usuarios  = mongoose.model('Usuarios');

let resp_ok = {
	error: false,
	codigo: 200,
	mensaje: ''
};

//GET - Se retornan todos los usuarios
exports.findAllUsuarios = (function(){
	Usuarios.find()
	.then(ret => {	
		console.log(ret)
		console.log('GET /usuarios')
		return ret;
	});
});

//GET - Consultar usuario por IDs, hay que cambiarlo por DNI
exports.findById = function(req, res) {
	Usuarios.findById(req.params.id, function(err, usuario) {
    if(err) return res.send(500, err.message);

    console.log('GET /usuarios/' + req.params.id);
		res.status(200).jsonp(usuario);
	});
};

//POST - Se inserta un usuario en la base de datos
exports.addUsuario = function(req, res) {
	console.log('POST');
	console.log(req.body);

	var usuario = new Usuarios({
		nombre:    req.body.nombre,
        apellido:  req.body.apellido,
        fechaAlta:   req.body.fechaAlta,
        dni:      req.body.dni,
        email:  req.body.email
	});

	usuario.save(function(err, usuario) {
		if(err) return(500, err.message);
	});

	return(resp_ok);
};

//PUT - Update a register already exists
exports.updateUsuario = function(req, res) {
	Usuarios.findById(req.params.id, function(err, usuario) {
        usuario.nombre   = req.body.nombre;
        usuario.apellido  = req.body.apellido;
        usuario.fechaAlta = req.body.fechaAlta;
        usuario.dni = req.body.dni;
        usuario.email = req.body.email;

		usuario.save(function(err) {
			if(err) return res.send(500, err.message);
      res.status(200).jsonp(usuario);
		});
	});
};

//DELETE - Eliminar un usuario
exports.deleteUsuario = function(req, res) {
	Usuarios.findById(req.params.id, function(err, usuario) {
		usuario.remove(function(err) {
			if(err) return res.send(500, err.message);
      res.status(200);
		})
	});
};